package dk.xakeps.testfx;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class App extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Label label = new Label("Source code here:\nhttps://gitlab.com/Xakep_SDK/testfx");
        HBox box = new HBox();
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        box.getChildren().add(vBox);
        box.setAlignment(Pos.CENTER);
        box.setPrefSize(300, 200);
        box.getChildren().add(label);
        Scene scene = new Scene(box);
        primaryStage.setScene(scene);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }
}
